# TestFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Enviroment Variables

apiUrl - Used for setting up url for api
apiVersion - Used for mentioning version.

-  apiUrl: 'http://localhost:3000',
-  apiVersion:"api/v1"

## About App 

This repository contains the frontend code made in angular. This allow to perform crud ( Add, Edit, Delete, View ) for users.

## Extra

- Allow user to filter with start date, end date and search option.
- User can sort any column in asc, desc order.
- Pagination with lazy loading option.