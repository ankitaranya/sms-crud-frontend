import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UserService } from '@services/user.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { User } from '@models/user';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import {AddDialogComponent} from './dialogs/add/add.dialog.component';
import {EditDialogComponent} from './dialogs/edit/edit.dialog.component';
import {DeleteDialogComponent} from './dialogs/delete/delete.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, finalize, map, tap } from 'rxjs/operators';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test-frontend';
  displayedColumns = ['_id', 'city', 'price', 'status', 'color', 'start_date', 'end_date', 'actions'];
  exampleDatabase: UserService | null;
  index: number;
  id: number;
  isDisabled: boolean = true;
  dataSource: UsersDataSource;
  startDate = new FormControl('');
  endDate = new FormControl('');
  startDateValidation: Date = new Date();

  constructor(public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: UserService) { }

  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild('startdate', { static: true }) start_date: ElementRef;
  @ViewChild('enddate', { static: true }) end_date: ElementRef;

  ngOnInit() {
    this.dataSource = new UsersDataSource(this.dataService);
    this.dataSource.loadUsers('', '_id', 'asc', 1, 10);
  }

  ngAfterViewInit() {

    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 1;
          this.loadData();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 1);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadData())
      )
      .subscribe();
  }

  dateFilter() {
    this.loadData();
  }

  onDateChange() {
    this.startDateValidation = new Date(this.start_date.nativeElement.value);
  }

  checkDisable() {
    if (this.start_date.nativeElement.value != '' && this.end_date.nativeElement.value != '')
      this.isDisabled = false;
    else
      this.isDisabled = true;
  }

  resetFilter() { 
    this.startDate = new FormControl('');
    this.endDate = new FormControl('');
    this.isDisabled = true;
  }

  loadData() {

    this.dataSource = new UsersDataSource(this.dataService);
    this.dataSource.loadUsers(
      this.filter.nativeElement.value,
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.start_date.nativeElement.value,
      this.end_date.nativeElement.value,
    );

  }

  deleteItem(userObj) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      panelClass: 'app-full-bleed-dialog',
      data: userObj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
       this.loadData();
      }
    });
  }

  addNew() { 
    const dialogRef = this.dialog.open(AddDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData()
      }
    });
  }

  startEdit(user) {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: {
        _id: user._id,
        city: user.city,
        start_date: user.start_date,
        end_date: user.end_date,
        price: user.price,
        status: user.status,
        color: user.color
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData()
      }
    });
  }

}

class UsersDataSource implements DataSource<User> {

  private userSubject = new BehaviorSubject<User[]>([]);
  private totalUserCount = new BehaviorSubject<number[]>([]);
  private currentUserCount = new BehaviorSubject<number[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public userTotalCount = this.totalUserCount.asObservable();
  public userCount = this.currentUserCount.asObservable();

  constructor(private dataService: UserService) { }

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.userSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.userSubject.complete();
    this.loadingSubject.complete();
  }

  loadUsers(filter = '',
    sortColumn = "_id", sortDirection = 'asc', pageIndex = 1, pageSize = 10, startDate = '', endDate = '') {

    this.loadingSubject.next(true);

    this.dataService.getUsers(filter, sortColumn, sortDirection,
      pageIndex, pageSize, startDate, endDate).pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(user => {
        this.userSubject.next(user.data);
        this.totalUserCount.next(user.total);
        this.currentUserCount.next(user.results);
      });
  }
}