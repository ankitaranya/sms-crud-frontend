import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { User } from '@models/User';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  dataChange: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  // Temporarily stores data from dialogs
  dialogData: any;
  userUrl: string;

  constructor(private httpClient: HttpClient, private toasterService: ToastrService) {
    this.userUrl = `${environment.apiUrl}/${environment.apiVersion}/users/`;
  }

  get data(): User[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllUsers(): void {
    this.httpClient.get<User[]>(this.userUrl).subscribe(data => {
      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getUsers(
    filter = '', sortColumn = "_id", sortOrder = 'asc',
    pageNumber = 1, pageSize = 10, startDate = '', endDate = ''): Observable<any> {

    return this.httpClient.get(this.userUrl, {
      params: new HttpParams()
        .set('filter', filter)
        .set('sort', sortColumn)
        .set('sortOrder', sortOrder)
        .set('page', pageNumber.toString())
        .set('limit', pageSize.toString())
        .set('start_date', startDate.toString())
        .set('end_date', endDate.toString())
    }).pipe(
      map(res => res)
    );
  }

  addUser(user: User): void {
    this.httpClient.post(this.userUrl, user).subscribe(data => {
      this.toasterService.success('User added successfully.');
    },
      (err: HttpErrorResponse) => {
        this.toasterService.error('Error occurred. Details: ' + err.name + ' ' + err.message);
      });
  }

  updateUser(user: User): void {
    this.httpClient.patch(this.userUrl + user._id, user).subscribe(data => {
      this.toasterService.success('Successfully Updated', "Users ID : " + user._id);
    },
      (err: HttpErrorResponse) => {
        this.toasterService.error('Error occurred. Details: ' + err.name + ' ' + err.message);
      }
    );
  }

  deleteUser(id: number): void {
    this.httpClient.delete(this.userUrl + id).subscribe(data => {
      this.toasterService.success('Successfully deleted', "Users ID : " + id);
    },
      (err: HttpErrorResponse) => {
        this.toasterService.error('Error occurred. Details: ' + err.name + ' ' + err.message);
      }
    );
  }
}