import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, ElementRef, EventEmitter, Inject, Output, ViewChild } from '@angular/core';
import { UserService } from '@services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent {
  isDisabled: boolean;

  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: UserService) {

  }

  startDateValidation: Date = new Date(this.data.start_date)

  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();
  @ViewChild('startdate', { static: true }) startDate: ElementRef;
  @ViewChild('enddate', { static: true }) endDate: ElementRef;


  userForm = new FormGroup({
    _id: new FormControl(this.data._id),
    city: new FormControl(this.data.city, [Validators.required]),
    price: new FormControl(this.data.price, [Validators.required]),
    status: new FormControl(this.data.status, [Validators.required]),
    color: new FormControl(this.data.color, [Validators.required]),
    start_date: new FormControl(new Date(this.data.start_date), [Validators.required]),
    end_date: new FormControl(new Date(this.data.end_date))
  });
  
  formControlCheck = new FormControl('', [
    Validators.required
  ]);

  ngOnInit() {
    this.isDisabled = false;
  }

  getErrorMessage() {
    return this.formControlCheck.hasError('required') ? 'Required field' : '';
  }

  onDateChange() {
    this.startDateValidation = new Date(this.startDate.nativeElement.value);
  }

  checkDisable() {
    if (this.startDate.nativeElement.value != '' && this.endDate.nativeElement.value != '')
      this.isDisabled = false;
    else
      this.isDisabled = true;
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateEdit(): void {
    this.dataService.updateUser(this.userForm.value);
  }
}
