import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, ElementRef, EventEmitter, Inject, Output, ViewChild } from '@angular/core';
import { UserService } from '@services/user.service';
import { FormControl, Validators } from '@angular/forms';
import { User } from '@models/User';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {
  isDisabled: boolean = true;
  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
    public dataService: UserService) { }

  startDateValidation: Date = new Date();
  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();
  @ViewChild('startdate', { static: true }) start_date: ElementRef;
  @ViewChild('enddate', { static: true }) end_date: ElementRef;

  formControl = new FormControl('', [
    Validators.required
  ]);

  data = new User();

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' : '';
  }

  onDateChange() {
    this.startDateValidation = new Date(this.start_date.nativeElement.value);
  }

  checkDisable() {
    if (this.start_date.nativeElement.value != '' && this.end_date.nativeElement.value != '')
      this.isDisabled = false;
    else
      this.isDisabled = true;
  }
  
  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addUser(this.data);
  }
}
